require 'json'
$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "todos/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'todos'
  s.version     = Todos::VERSION
  s.authors     = ['Tobias Grasse']
  s.email       = ['tg@glancr.de']
  s.homepage    = 'https://glancr.de/modules/todos'
  s.summary     = 'mirr.OS widget to display to-do items.'
  s.description = "See your to-do's from compatible data sources."
  s.license     = "MIT"
  s.metadata    = { 'json' =>
                  {
                    type: 'widgets',
                    title: {
                      enGb: 'To-do\'s',
                      deDe: 'Aufgaben',
                      frFr: 'Tâches',
                      esEs: 'Tareas',
                      plPl: 'Zadania',
                      koKr: '작업'
                    },
                    description: {
                      enGb: s.description,
                      deDe: 'Zeige deine Aufgaben aus kompatiblen Datenquellen an.',
                      frFr: 'Voyez vos tâches à faire à partir de sources de données compatibles.',
                      esEs: 'Vea sus tareas desde fuentes de datos compatibles.',
                      plPl: 'Zobacz swoje zadania do wykonania z kompatybilnych źródeł danych.',
                      koKr: '호환 가능한 데이터 소스에서 할일을 확인하십시오.'
                    },
                    sizes: [
                      {
                        w: 6,
                        h: 4
                       }
                    ],
                    languages: [:enGb, :deDe],
                    group: 'reminder_list',
                    compatibility: '0.9.0'
                  }.to_json
                }

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_development_dependency 'rails'
end
