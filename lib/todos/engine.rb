module Todos
  class Engine < ::Rails::Engine
    isolate_namespace Todos
    config.generators.api_only = true
  end
end
