# frozen_string_literal: true

module Todos
  class Configuration < WidgetInstanceConfiguration
    attribute :sort, :string, default: "created"

    validates :sort, inclusion: %w[created created_reverse alphabetical alphabetical_reverse]
  end
end
